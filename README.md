# README #



The following HTLM5 web based application was developed over 24 hrs as part of the Deutsche Bank COO Hackathon. 

It was developed by a team of 9 people, 8 of which worked at DB at the time and one of which was an undergraduate student selectively invited to take part in the hackathon. 

**Try it yourself**
http://webprojects.eecs.qmul.ac.uk/tc304/DBHackathon/emotion.html


The team was formed of: 



Ravinder Assi (Business Analyst at DB) 


Thomas Chalmers (Undergraduate at Queen Mary University of London) 


Juliusz Wiatrak (Graduate Analyst at DB)
 [Team Leader]
Tony Chen (Graduate Analyst at DB)

Teodor Popescu (Graduate Analyst at DB)

Cannis Chan (Graduate Analyst at DB)

Juno Wo (Graduate Analyst at DB)

The web application optimised for iPhone 6/7 plus, however is responsive to all mobile and desktop devices.